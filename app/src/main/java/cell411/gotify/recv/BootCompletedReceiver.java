package cell411.gotify.recv;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class BootCompletedReceiver extends BroadcastReceiver {

  @Override
  public void onReceive(Context context, Intent intent) {
    String action = intent.getAction();
    if(action.equals("android.intent.action.BOOT_COMPLETED")) {
      context.startForegroundService(new Intent(context, GotifyService.class));
    } else {
      Toast.makeText(context.getApplicationContext(), "Unexpected Action Ignored",Toast.LENGTH_LONG).show();
    }
  }

}
