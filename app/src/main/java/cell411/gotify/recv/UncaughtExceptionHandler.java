package cell411.gotify.recv;

public class UncaughtExceptionHandler {
  public static final String TAG = Util.getTag();

  public static void registerCurrentThread() {
    Thread.setDefaultUncaughtExceptionHandler((t, e) -> Log.e(TAG,"uncaught exception", e));
  }
}
