package cell411.gotify.recv;

import org.json.JSONException;
import org.json.JSONObject;

public class Message {
  final int mId;
  final int mAppId;
  final String mTitle;
  final String mMessage;
  final int mPriority;

  public Message(String message) {
    this(null,message);
  }

  public Message(int id, int appId, String title, String message, int priority) {
    mId=id;
    mAppId=appId;
    if(message==null)
      message="";
    mMessage=message;
    if(title==null)
      title="";
    mTitle=title;
    mPriority=priority;
  }

  public Message(String title, String message) {
    this(0,0,title,message,5);
  }

  public String toJSON() {
    try {
      JSONObject jsonObject = new JSONObject()
        .put("id", mId)
        .put("appId", mAppId)
        .put("message", mMessage)
        .put("priority", mPriority)
        .put("title", mTitle);

      return jsonObject.toString(2);
    } catch (JSONException e) {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }
  public static Message fromJSON(String json) {
    try {
      JSONObject jsonObject = new JSONObject(json);
      int id = jsonObject.optInt("id", 0);
      int appId = jsonObject.optInt("appId", 0);
      String message = jsonObject.optString("message","");
      String title = jsonObject.optString("title","");
      int priority = jsonObject.optInt("priority", 5);
      return new Message(id, appId, title,message, priority);
    } catch (JSONException e) {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }
  static {
    Message message1, message2;
    message1 = new Message(300,301,"title","message",302);
    message2 = fromJSON(message1.toJSON());
    message1.assertEqual(message2);
    message1 = new Message("message");
    message2 = fromJSON(message1.toJSON());
    message1.assertEqual(message2);
  }

  private static void assertEqual(String lhs, String rhs) {
    if(lhs==null)
      assert rhs == null;
    else
      assert(lhs.equals(rhs));
  }
  private void assertEqual(Message message2) {
    assertEqual(mMessage,message2.mMessage);
    assertEqual(mTitle,message2.mTitle);
    assert mId == message2.mId;
    assert mAppId == message2.mAppId;
    assert mPriority==message2.mPriority;
  }
}