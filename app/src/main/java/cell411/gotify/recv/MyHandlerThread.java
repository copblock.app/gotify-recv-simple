package cell411.gotify.recv;

import android.os.Handler;
import android.os.HandlerThread;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.collection.ArraySet;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.ws.WebSocket;
import okhttp3.ws.WebSocketCall;
import okhttp3.ws.WebSocketListener;
import okio.Buffer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import static okhttp3.ws.WebSocket.BINARY;
import static okhttp3.ws.WebSocket.TEXT;

class MyHandlerThread extends HandlerThread {
  public static final String TAG = Util.getTag();

  private String mToken;
  private final OkHttpClient mClient;
  private final Runnable mConnect = this::connect;
  private final Runnable mPlayMusic;
  private MessageListener mDelegateListener = this::onMessage;
  private static MessageListener mMessageListener;
  private Handler tmpHandler;
  private final Handler mHandler;
  private final ArrayList<Message> mMessages = new ArrayList<>();
  private final ArraySet<ResponseBody> mRawBodies = new ArraySet<>();

  static void setTextListener(MessageListener messageListener) {
    mMessageListener = messageListener;
  }

  void setToken(String token) {
    mToken=token;
    mHandler.post(mConnect);
  }
  public MyHandlerThread(Runnable playMusic) {
    super("Gotify Thread");
    mPlayMusic=playMusic;
    synchronized(this) {
      start();
      while (tmpHandler == null) {
        try {
          wait(10);
        } catch (InterruptedException ie) {
          ie.printStackTrace();
        }
      }
      mHandler=tmpHandler;
    }
    mClient = new OkHttpClient.Builder()
      .readTimeout(0, TimeUnit.MILLISECONDS)
      .build();
  }

  public synchronized Handler getHandler() {
    return mHandler;
  }

  interface MessageListener {
    void onMessage(Message message);
  }

  private void onMessage(Message message) {
    MessageListener listener = mMessageListener;
    if(listener ==null) {
      mHandler.post(mPlayMusic);
      mMessages.add(message);
    } else if ( mMessages.size() > 0 ) {

      mMessages.add(message);

      while(mMessages.size()>0 && mMessageListener!=null) {
        mHandler.post(mPlayMusic);
        message = mMessages.get(0);
        mMessages.remove(0);
        listener.onMessage(message);
      }
    } else {
      mHandler.post(mPlayMusic);
      listener.onMessage(message);
    }
  }


  protected synchronized void onLooperPrepared() {
    super.onLooperPrepared();
    tmpHandler = new Handler(getLooper());
  }

  public void connect() {
    if(mToken==null || mToken.isEmpty()) {
      Toast.makeText(MainActivity.getContext(), "Your token in blank.  Fix it!", Toast.LENGTH_LONG).show();
      mHandler.postDelayed(mConnect,15000);
    }else{
      Request.Builder builder = new Request.Builder();
      builder
        .url("wss://dev.copblock.app/gotify/stream")
        .addHeader("X-Gotify-Key", mToken)
        .build();

      Request request = builder.build();
      WebSocketCall.create(mClient, request).enqueue(new MyListener(mDelegateListener));
    }
  }

  class MyListener implements WebSocketListener {
    public MyListener(@NonNull MessageListener messageListener) {
      mDelegateListener = messageListener;
      mDelegateListener.onMessage(new Message("Listener Created"));
    }

    @Override
    public void onOpen(WebSocket webSocket, Response response) {
      mDelegateListener.onMessage(new Message("onOpen Called"));
    }

    @Override
    public void onFailure(IOException e, Response response) {
      mDelegateListener.onMessage(new Message("onFailure Called: " + e));
      mHandler.postDelayed(mConnect,500);
    }

    @Override
    public void onMessage(ResponseBody body) throws IOException {
      MediaType contentType = body.contentType();
      Message message;
      mRawBodies.add(body);
      Log.i(TAG, mRawBodies.size()+" bodies");
      if (contentType == TEXT) {
        String str = body.string().trim();
        if(str.startsWith("{") && str.endsWith("}")) {
          message=Message.fromJSON(str);
        } else {
          message=new Message(str);
        }
      } else if (contentType == BINARY) {
        message=new Message("Error", "Binary: "+body.source().readByteString().hex());
      } else {
        message=new Message("Error", "MESSAGE:  <With unexpected content type");
      }
      mDelegateListener.onMessage(message);
    }

    @Override
    public void onPong(Buffer payload) {
      mDelegateListener.onMessage(new Message("PONG: " + payload.readUtf8()));
    }

    @Override
    public void onClose(int code, String reason) {
      mDelegateListener.onMessage(new Message("onClose called: reason=" + reason));
    }
  }
}

