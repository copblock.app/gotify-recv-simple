package cell411.gotify.recv;

@SuppressWarnings("unused")


public class Util {
  public static final String TAG = currentSimpleClassName();

  private static String currentSimpleClassName() {
    return currentSimpleClassName(1);
  }

  private static String currentMethodName() {
    return currentMethodName(1);
  }

  public static String currentMethodName(int i) {
    return currentStackPos(i).getMethodName();
  }

  public static String currentSimpleClassName(int i) {
    String fullClassName = currentStackPos(i).getClassName();
    int pos = fullClassName.lastIndexOf('.');
    return fullClassName.substring(pos);
  }

  static StackTraceElement currentStackPos(int i) {
    Exception ex = new Exception();
    StackTraceElement[] trace = ex.getStackTrace();
    if (trace.length < 3+i)
      throw new RuntimeException("trace.length<"+(3+i)+"!");
    return trace[2+i];
  }

  public static String getTag() {
    return currentSimpleClassName(1);
  }
}

