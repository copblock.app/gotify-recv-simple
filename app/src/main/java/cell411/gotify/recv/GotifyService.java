package cell411.gotify.recv;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.IBinder;
import androidx.core.app.NotificationCompat;

public class GotifyService extends Service {
  public static final String TAG = Util.getTag();

  static {
    Log.i(TAG, "Loading Class");
  }

  final MyHandlerThread mThread;
  final Handler mHandler;
  private MediaPlayer mMediaPlayer;

  Runnable mPlayMusic = this::bbbbbBad;

  public GotifyService() {
    mThread = new MyHandlerThread(mPlayMusic);
    mHandler = mThread.getHandler();
  }

  @Override
  public void onCreate() {
    super.onCreate();
    mMediaPlayer = MediaPlayer.create(this, R.raw.bad_to_the_bone);
  }

  @Override
  public int onStartCommand(Intent intent, int flags, int startId) {
    try {
      SharedPreferences prefs = getSharedPreferences("gotify", MODE_PRIVATE);
      String token = prefs.getString("token", "");
      // To bootstrap the thing, put your token as the defValue above, and run it once
      // to let it save.  Then remove the defValue.
      prefs.edit().putString("token", token).apply();
      mThread.setToken(token);

      super.onStartCommand(intent, flags, startId);

      mHandler.post(this::startPushService);
      return START_STICKY;
    } catch (Throwable e) {
      e.printStackTrace();
      throw new RuntimeException("Starting Service", e);
    }
  }

  private void bbbbbBad() {
    if(mMediaPlayer == null || mMediaPlayer.isPlaying())
      mHandler.postDelayed(mPlayMusic, 3000);
    else
      mMediaPlayer.start();
  }


  private void startPushService() {
    UncaughtExceptionHandler.registerCurrentThread();
    showForegroundNotification(getString(R.string.websocket_init), "Initializing");

    NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    NotificationSupport.createChannels(nm);

  }

  public void showForegroundNotification(String title, String message) {
    Intent notificationIntent = new Intent(this, MainActivity.class);

    PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
    NotificationCompat.Builder notificationBuilder =
      new NotificationCompat.Builder(this, NotificationSupport.Channel.FOREGROUND);
    notificationBuilder.setSmallIcon(R.mipmap.appicon);
    notificationBuilder.setOngoing(true);
    notificationBuilder.setPriority(NotificationCompat.PRIORITY_MIN);
    notificationBuilder.setShowWhen(false);
    notificationBuilder.setWhen(0);
    notificationBuilder.setContentTitle(title);
    notificationBuilder.setChannelId(NotificationSupport.Channel.FOREGROUND);

    if (message != null) {
      notificationBuilder.setContentText(message);
      notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
    }
    if(pendingIntent!=null)
      notificationBuilder.setContentIntent(pendingIntent);

    startForeground(NotificationSupport.ID.FOREGROUND, notificationBuilder.build());
  }

  @Override
  public IBinder onBind(Intent intent) {
    return null;
  }
}
