package cell411.gotify.recv;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.lang.ref.WeakReference;

public class MainActivity extends AppCompatActivity {

  private EditText mEditText;
  private Button mSendText;
  private LinearLayout mScroller;

  static WeakReference<Context> smContext;
  static Context getContext() {
    if(smContext==null)
      return null;
    return smContext.get();
  }
  final Handler mHandler = new Handler(Looper.getMainLooper());
  private View mClear;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    smContext = new WeakReference<>(getApplicationContext());
    setContentView(R.layout.activity_main);
    Intent intent = new Intent(this, GotifyService.class);

    startForegroundService(intent);
    mEditText = findViewById(R.id.edit_text);
    mClear = findViewById(R.id.clr_text);
    mSendText = findViewById(R.id.send_text);
    mScroller = findViewById(R.id.scroller);

    mSendText.setOnClickListener(this::onSendTextClick);
    mClear.setOnClickListener(this::onClearClick);
  }

  protected void onPause() {
    super.onPause();
    MyHandlerThread.setTextListener(null);
  }
  protected void onResume() {
    super.onResume();
    MyHandlerThread.setTextListener(this::onMessage);
  }
  private void onClearClick(View view) {
    assert view == mClear;
    Log.i(TAG, "Clear Clicked");
    mScroller.removeAllViews();
  }


  public static final String TAG = MainActivity.class.getSimpleName();

  public void onSendTextClick(View v) {
    assert v == mSendText;
    Log.i(TAG, "Send Text Clicked");
    String text = mEditText.getText().toString();
    if (text.length() == 0)
      return;
    mEditText.setText("");
    Log.i(TAG, "  text=" + text);
  }


  void onMessage(Message message) {
    if (!Looper.getMainLooper().isCurrentThread()) {
      mHandler.post(() -> MainActivity.this.onMessage(message));
      return;
    }

    LayoutInflater inflater = getLayoutInflater();
    View view = inflater.inflate(R.layout.message, mScroller, false);
    TextView titleView = view.findViewById(R.id.title);
    TextView messageView = view.findViewById(R.id.message);

    if(isNoE(message.mTitle) && isNoE(message.mMessage)) {
      messageView.setVisibility(View.GONE);
      titleView.setText(R.string.empty_message);
    } else {
      if (isNoE(message.mTitle))
        titleView.setVisibility(View.GONE);
      else
        titleView.setText(message.mTitle);

      if (isNoE(message.mMessage))
        messageView.setVisibility(View.GONE);
      else
        messageView.setText(message.mMessage);
    }
    FrameLayout layout = new FrameLayout(this);
    layout.setPadding(2,2,2,2);
    layout.setBackgroundColor(Color.BLACK);
    layout.addView(view);
    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
      ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    params.setMargins(2,2,2,2);
    layout.setLayoutParams(params);
    view.setBackgroundColor(Color.WHITE);
    mScroller.addView(layout, 0);
  }

  private boolean isNoE(String mTitle) {
    return mTitle==null || mTitle.isEmpty();
  }
}